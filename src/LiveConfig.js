import React from "react"
import ReactDOM from "react-dom"
import LiveConfig from "./components/LiveConfig/LiveConfig"

ReactDOM.render(
  <LiveConfig />,
  document.getElementById("root")
)