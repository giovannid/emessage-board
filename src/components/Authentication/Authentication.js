import jwt  from 'jsonwebtoken';

export default class Authentication {

    constructor(token, opaque_id, clientId) {
        this.state = {
            token,
            opaque_id,
            user_id: false,
            isMod: false,
            role: "",
            clientId
        }
    }

    isLoggedIn() {
        return this.state.opaque_id[0] === 'U' ? true : false;
    }

    isBroadcaster() {
        return this.state.role === "broadcaster" ? true : false;
    }

    isModerator() {
        return this.state.isMod;
    }

    hasSharedId() {
        return this.state.user_id;
    }

    getUserId() {
        return this.state.user_id;
    }

    getOpaqueId() {
        return this.state.opaque_id;
    }

    getClientId() {
        return this.state.clientId;
    }
    
    setToken(token, opaque_id, clientId) {
        let isMod = false;
        let role = "";
        let user_id = "";

        try {
            let decoded = jwt.decode(token);
            
            if(decoded.role === "broadcaster" || decoded.role === "moderator"){
                isMod = true;
            }

            user_id = decoded.user_id;
            role = decoded.role;
        } catch(e) {
            token= "";
            opaque_id= "";
        }

        this.state = {
            token,
            opaque_id,
            isMod,
            user_id,
            role,
            clientId
        }
    }

    isAuthenticated() {
        if(this.state.token && this.state.opaque_id) {
            return true;
        } else {
            return false;
        }
    }

    makeCall(url, method = "GET") {
        return new Promise((resolve, reject) => {
            if(this.isAuthenticated()) {
                let headers = {
                    'Content-Type':'application/json',
                    'Authorization': `Bearer ${this.state.token}`
                }
    
                fetch(url,
                    {
                        method,
                        headers,
                    })
                    .then(response => resolve(response))
                    .catch(e => reject(e));
            } else {
                reject('Unauthorized');
            }
        });
    }
}