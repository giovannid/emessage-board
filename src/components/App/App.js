import React from 'react';
import Authentication from '../Authentication/Authentication';
import MessageList from './MessageList';

import './App.css';
// EMessage Board
export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.Authentication = new Authentication();

        this.twitch = window.Twitch ? window.Twitch.ext : null;
        this.state = {
            finishedLoading: false,
            theme: 'dark',
            messages: []
        }
    }

    contextUpdate(context, delta) {
        if(delta.includes('theme')) {
            this.setState({
                theme: context.theme
            });
        }
    }

    componentDidMount() {
        if(this.twitch) {
            this.twitch.onContext((context,delta) => {
                this.contextUpdate(context,delta)
            });
            
            this.twitch.onAuthorized(auth => {
                this.Authentication.setToken(auth.token, auth.userId, auth.clientId);
                if(!this.state.finishedLoading) {
                    this.setState({
                        finishedLoading: true
                    });
                }
            });

            this.twitch.listen('broadcast', (target, type, c) => {
                try {
                    let parsedContent = JSON.parse(c);
                    this.setState({
                        messages: parsedContent,
                    });
                } catch(e) {
                    console.error(`Error parsing message! ${e}`);
                }
            });
        }
    }

    componentWillUnmount() {
        if(this.twitch){
            this.twitch.unlisten('broadcast');
        }
    }
    
    render() {
        if(this.state.finishedLoading){
            return (
                <div className={`VideoComponent ${!this.state.theme === 'light' ? 'VideoComponent-light' : 'VideoComponent-dark'}`}>
                    <MessageList messages={this.state.messages} />
                </div>
            )
        } else {
            return (
                <div className={`VideoComponent ${this.state.theme === 'light' ? 'VideoComponent-light' : 'VideoComponent-dark'}`}>
                    <p>Loading...</p>
                </div>
            )
        }
    }
}