import React from 'react'
import * as ReactDOM from 'react-dom';
import MessageItem from './MessageItem';

import { NodeGroup } from 'react-move'

export default class MessageList extends React.Component{
    constructor(props) {
        super(props);
        this.messageList = React.createRef();
    }
    scrollToBottom() {
        const messageList = this.messageList.current;
        const scrollHeight = messageList.scrollHeight;
        const height = messageList.clientHeight;
        const maxScrollTop = scrollHeight - height;
        ReactDOM.findDOMNode(messageList).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    }

    componentDidMount() {
        if(this.messageList && this.props.messages.length > 0) this.scrollToBottom();
    }

    componentDidUpdate() {
        if(this.props.messages.length > 0) this.scrollToBottom();
    }

    render() {
        return (
            <div className="MessageContainer">
                    {this.props.messages.length > 0 ? (
                        <div className="MessageList"  ref={this.messageList}>
                            <NodeGroup 
                                data={this.props.messages} 
                                keyAccessor={m => m.id}
                                start={() => ({
                                    opacity: 1e-6,
                                })}
                                enter={(d) => ({
                                    opacity: [1],
                                    timing: { duration: 500 },
                                })}
                            >
                            {nodes => (
                                <React.Fragment>
                                    {nodes.map(({key, data, state: { opacity }}) => <MessageItem message={data} op={opacity} key={key} />)}
                                </React.Fragment>
                            )}
                            </NodeGroup>
                        </div>
                    ) : (
                        <div className="NoMessages">
                            <div>   
                                <svg  width="200" height="200" viewBox="0 0 130 130" xmlns="http://www.w3.org/2000/svg"><g data-name="Layer 2" id="Layer_2"><g id="Export"><path className="cls-1" d="M64,3A61,61,0,1,1,3,64,61.06,61.06,0,0,1,64,3m0-3a64,64,0,1,0,64,64A64,64,0,0,0,64,0Z"/><path className="cls-1" d="M86.26,94.81a1.47,1.47,0,0,1-.85-.26,38,38,0,0,0-24.09-6.7A37.48,37.48,0,0,0,42.6,94.54a1.5,1.5,0,1,1-1.72-2.46,40.5,40.5,0,0,1,20.23-7.22,41,41,0,0,1,26,7.22,1.5,1.5,0,0,1-.85,2.73Z"/><path className="cls-1" d="M43.83,60.63a9.34,9.34,0,0,1-8.28-5,1.5,1.5,0,1,1,2.64-1.41,6.39,6.39,0,0,0,11.27,0,1.5,1.5,0,1,1,2.65,1.41A9.37,9.37,0,0,1,43.83,60.63Z"/><path className="cls-1" d="M85.57,60.63a9.34,9.34,0,0,1-8.28-5,1.5,1.5,0,1,1,2.64-1.41,6.39,6.39,0,0,0,11.27,0,1.5,1.5,0,1,1,2.64,1.41A9.34,9.34,0,0,1,85.57,60.63Z"/></g></g></svg>
                                <p>No messages.</p>
                            </div>
                        </div>
                    )}
                </div>
        )
    }
}