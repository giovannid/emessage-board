import React from 'react'
import LinkifyReact from 'linkifyjs/react';
import * as linkify from 'linkifyjs';

export default class MessageItem extends React.Component{
    isSingleImage(text) {
        let findImgLink = linkify.find(text);
        if(findImgLink.length === 1) {
            let onlyImage = text.trim().replace(findImgLink[0]['href'], '').trim();
            if(onlyImage.length === 0) {
                let regex = /(?:png|jpg|jpeg|gif)/gi;
                let matchImage = `${findImgLink[0]['href']}`.match(regex);
                if(matchImage !== null && matchImage.length === 1) {
                    return true;
                }
            }
        }
        return false;
    }
    render() {
        let { text } = this.props.message;
        let isSingleImage = this.isSingleImage(text);
        return (
            <div className="MessageItem" style={{ opacity: this.props.op }}>
                {isSingleImage ? (
                    <img src={text} border="0" style={{maxWidth: '100%'}} />
                ) : (
                    <LinkifyReact tagName="div" className="MessageBody">{text}</LinkifyReact>
                )}
            </div>
        )
    }
}