import React from 'react'
import * as ReactDOM from 'react-dom';

export default class MessageList extends React.Component{
    constructor(props) {
        super(props);
        this.messageList = React.createRef();
    }
    scrollToBottom() {
        const messageList = this.messageList.current;
        const scrollHeight = messageList.scrollHeight;
        const height = messageList.clientHeight;
        const maxScrollTop = scrollHeight - height;
        ReactDOM.findDOMNode(messageList).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        let { current_index, timer_queue } = this.props;
        return (
            <div className="MessageContainer">
                <div className="MessageList" ref={this.messageList}>
                    {this.props.messages.length > 0 ? (
                        <React.Fragment>
                            {this.props.messages.map((message, index) => (
                                <div className={`MessageItem ${timer_queue && current_index === index ? 'current_msg' : ''}`} key={message.id}>
                                    <div className="MessageRemove">
                                        <button onClick={_ => { this.props.removeMessage(message.id, timer_queue); }}>
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFQSURBVEhL7ZXNSsNAFIXzJPoQkqALFzZFEFzoQhR0Ib6d4EZc2QpW7bJYETf+vIHgptMBaeM493pHMnOmkUTd9cAhkDn3O5kJJMlctaTXVhZVK+uOWtm7yjNTxzxjZ3V7aUFwKBu4iA3Xsi0RHKrJk4cmhuBQsQHVXsZ7P1hwqDCoj/ZMMRgYfbDt3SePt9bN5OyUr+Ga4FBhkADm9c18PD57JQQtboe8RpnyDFlwqDCoNlbNtHfplZThdP3VDtjlkqcXU9w/VMLJgkPFwmwq6fcZzHBbMgtOFhwqFibzsQzvvgtoJ3ofX7yz4FCxsHfmtsTthN/JjBLBocKgB3dnTsd11assERwqDDoQvNBSyfTm2pshCw4VBvXhDoM8uLMtmZwcm/HuJqwJDhUGm1pwqP//2P3B53qUp+eCQ9HPQuVpp8lOvmbSTuUPZy5UknwC8OdJutVjjxEAAAAASUVORK5CYII=" />
                                        </button>
                                    </div>
                                    <div className="MessageBody">{message.text}</div>
                                </div>
                                )
                            )}
                        </React.Fragment>
                    ) : (
                        <p>No messages.</p>
                    )}
                </div>
            </div>
        )
    }
}