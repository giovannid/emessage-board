import React from 'react'
import { setInterval } from 'core-js';

export default class TimerSelection extends React.Component{

    handleIntervalChange(action) {

        if(action === "add") {
            this.props.updateInverval(this.props.interval + 1);
        } else if (action === "remove") {
            let newValue = (this.props.interval - 1) < 1 ? 1 : this.props.interval - 1;
            this.props.updateInverval(newValue);
        }

    }

    handleRepeatChange(e) {
        this.props.updateRandomize();
    }

    render() {
        // let { repeat } = this.props;
        return (
            <div className="TimerSelection">
                <div className="NextIn">
                    <span>Change Every (min)</span>
                    <div className="NextInAdd" onClick={_ => this.handleIntervalChange("add")}>
                        <svg width="22px" height="23px" viewBox="0 0 22 23" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <g id="Group" fillRule="nonzero" fill="#6441A4">
                                    <path d="M11,0.20625 C4.95,0.20625 0,5.15625 0,11.20625 C0,17.25625 4.95,22.20625 11,22.20625 C17.05,22.20625 22,17.25625 22,11.20625 C22,5.0875 17.05,0.20625 11,0.20625 Z M11,19.45625 C6.4625,19.45625 2.75,15.74375 2.75,11.20625 C2.75,6.66875 6.4625,2.95625 11,2.95625 C15.5375,2.95625 19.25,6.66875 19.25,11.20625 C19.25,15.74375 15.5375,19.45625 11,19.45625 Z" id="Shape"></path>
                                    <polygon id="Shape" points="12.375 7.08125 9.625 7.08125 9.625 9.83125 6.875 9.83125 6.875 12.58125 9.625 12.58125 9.625 15.33125 12.375 15.33125 12.375 12.58125 15.125 12.58125 15.125 9.83125 12.375 9.83125"></polygon>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div className="NextInCurrent">{this.props.interval}</div>
                    <div className="NextInRemove" onClick={_ => this.handleIntervalChange("remove")}>
                        <svg width="22px" height="23px" viewBox="0 0 22 23" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <g id="subtrsct2" fillRule="nonzero" fill="#6441A4">
                                    <path d="M11,0.20625 C4.95,0.20625 0,5.15625 0,11.20625 C0,17.25625 4.95,22.20625 11,22.20625 C17.05,22.20625 22,17.25625 22,11.20625 C22,5.0875 17.11875,0.20625 11,0.20625 Z M11,19.45625 C6.4625,19.45625 2.75,15.74375 2.75,11.20625 C2.75,6.66875 6.4625,2.95625 11,2.95625 C15.5375,2.95625 19.25,6.66875 19.25,11.20625 C19.25,15.74375 15.60625,19.45625 11,19.45625 Z" id="Shape"></path>
                                    <rect id="Rectangle-path" x="6.875" y="9.83125" width="8.25" height="2.75"></rect>
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>
                {/* <div className="Repeat">
                    <div className="container">
                        <input type="checkbox" id="repeat_checkbox" defaultChecked={repeat} onChange={this.handleRepeatChange.bind(this)}/>
                        <label htmlFor="repeat_checkbox" className="repeat_label"></label>
                    </div>
                    <span className="title">Randomize</span>
                </div> */}
            </div>
        );
    }
}