import React from 'react'
import MessageList from './MessageList';
import MessageForm from './MessageForm';
import TimerSelection from './TimerSelection';

export default class TimerMessages extends React.Component{
    render() {
        let { messages, interval, repeat, current_index, sendMessage, removeMessage, updateInverval, updateRandomize } = this.props;
        return (
            <div className="TimerMessages">
                <MessageList messages={messages} removeMessage={removeMessage} current_index={current_index} timer_queue={true} />
                <TimerSelection interval={interval} repeat={repeat} updateInverval={updateInverval} updateRandomize={updateRandomize} />
                <MessageForm sendMessage={sendMessage} timer_queue={true} />
            </div>
        );
    }
}