import React from 'react'
import MessageList from './MessageList';
import MessageForm from './MessageForm';

export default class TextMessages extends React.Component{
    render() {
        const { messages, removeMessage, sendMessage } = this.props;
        return (
            <div className="TextMessages">
                <MessageList messages={messages} removeMessage={removeMessage} />
                <MessageForm sendMessage={sendMessage} />
            </div>
        );
    }
}