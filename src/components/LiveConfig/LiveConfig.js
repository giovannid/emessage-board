import React from 'react'
import jwt  from 'jsonwebtoken';
import short from "short-uuid";
import { debounce } from 'lodash';

import Authentication from '../Authentication/Authentication'
import TextMessages from './TextMessages';
import TimerMessages from './TimerMessages';

import './LiveConfig.css'

export default class LiveConfigPage extends React.Component{
    constructor(props) {
        super(props);
        this.Authentication = new Authentication();
        this.twitch = window.Twitch ? window.Twitch.ext : null;

        this.state = {
            finishedLoading: false,
            theme:'dark',
            messages: [],
            timer_interval: 1,
            timer_repeat: false,
            timer_next_index: 0,
            timer_current_index: null,
            timer_messages: [],
            timer_last_checked: '',
            tab: 'text',
            removeAllConfirmation: false,
        }
    }

    contextUpdate(context, delta){
        if(delta.includes('theme')){
            this.setState({
                theme: context.theme
            });
        }
    }

    componentDidMount() {
        if(this.twitch) {
            this.twitch.onContext((context, arr) => {
                this.contextUpdate(context, arr);
            });

            this.twitch.onAuthorized(auth => {
                this.Authentication.setToken(auth.token, auth.userId, auth.clientId);
                if(!this.state.finishedLoading){
                    this.setState({
                        finishedLoading:true,
                        timer_last_checked: Date.now()
                    }, _ => {
                        this.startWatch();
                    });
                }
            });
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    startWatch() {
        this.interval = setInterval(_ => {
            let { timer_messages, timer_next_index, timer_interval, timer_last_checked } = this.state;

            if(timer_messages.length > 0 && Date.now() - timer_last_checked >= 1000*60*timer_interval) {
                let payload = {
                    timer_next_index: timer_next_index + 1,
                    timer_current_index: timer_next_index
                }
                
                payload.timer_message = timer_messages[timer_next_index] ? [timer_messages[timer_next_index]] : [];
                
                if(payload.timer_message.length === 0 && timer_messages.length > 1) {
                    payload.timer_message = timer_messages[0] ? [timer_messages[0]] : [];
                    
                    if(payload.timer_message.length === 1) {
                        payload.timer_current_index = 0;
                        payload.timer_next_index = 1;
                    }
                }
                
                if(payload.timer_message.length === 1) {
                    this.setState(prevState => {
                        payload.messages = prevState.messages.filter(msg => msg.timer_message === undefined);
                        return {
                            messages: [...payload.messages, ...payload.timer_message],
                            timer_current_index: payload.timer_current_index,
                            timer_next_index: payload.timer_next_index,
                        }
                    },  _ => {
                        this.sendPubSubMessage();
                    });
                }
                this.setState({ timer_last_checked: Date.now() });
            } else {
                if(this.state.messages.length > 0) {
                    this.sendPubSubMessage();
                }
            }
        }, 10000);
    }

    sendPubSubMessage(callback) {
        debounce(_ => {
            if(this.Authentication.isAuthenticated) {
                this.twitch.send('broadcast', 'application/json', JSON.stringify(this.state.messages));
                callback && typeof callback === "function" && callback();
            }
        }, 1000)();
    }

    addNewMessage(message_text, timer_queue = false) {
        this.setState(prevState => {
            let newId = short.generate();
            let message = {
                id: newId,
                text: message_text,
                createdAt: Date.now()
            }
            if(timer_queue) {
                message.timer_message = true;
                return {
                    timer_messages: [...prevState.timer_messages, message],
                    timer_last_checked: Date.now(),
                    removeAllConfirmation: false
                }
            }
            return {
                messages: [...prevState.messages, message],
                removeAllConfirmation: false
            }
        }, _ => {
            if(!timer_queue) {
                this.sendPubSubMessage();
            }
        });
    }

    removeMessage(msg_id, timer_queue = false) {
        this.setState(prevState => {
            if(timer_queue) {
                return {
                    timer_messages: prevState.timer_messages.filter(msg => msg.id !== msg_id),
                    removeAllConfirmation: false
                }
            }
            return {
                messages: prevState.messages.filter(msg => msg.id !== msg_id),
                removeAllConfirmation: false
            }
        }, _ => {
            if(!timer_queue) {
                this.sendPubSubMessage();
            }
        });
    }

    handleTabChange(tab) {
        this.setState({ tab: tab });
    }

    handleRemoveAllClick(choice) {
        this.setState({
            removeAllConfirmation: !choice
        });
    }

    handleRemoveAll() {
        this.setState({
            messages: [],
            timer_messages: [],
            timer_next_index: 0,
            timer_current_index: null,
            timer_last_checked: Date.now(),
            removeAllConfirmation: false
        }, _ => {
            this.sendPubSubMessage();
        });
    }

    handleTimerIntervalUpdate(interval) {
        if(parseInt(interval, 10) === NaN || interval === 1 || interval === "") interval = 1;
        this.setState(({
            timer_interval: interval,
            timer_next_index: 0
        }));
    }

    handleRandomize() {
        this.setState(({ timer_repeat }) => ({
            timer_repeat: !timer_repeat,
            timer_next_index: 0
        }));
    }

    render() {
        let { tab, removeAllConfirmation, timer_current_index, timer_repeat, timer_interval, timer_messages } = this.state;
        if(this.state.finishedLoading && this.Authentication.isBroadcaster()){
            return(
                <div className="Config">
                    <div className={`LiveConfigPage ${this.state.theme === 'light' ? 'LiveConfigPage-light' : 'LiveConfigPage-dark'}`}>
                        {tab === 'text' ? (
                            <TextMessages 
                                messages={this.state.messages} 
                                removeMessage={this.removeMessage.bind(this)} 
                                sendMessage={this.addNewMessage.bind(this)} />
                        ) : (
                            <TimerMessages 
                                updateRandomize={this.handleRandomize.bind(this)} 
                                updateInverval={this.handleTimerIntervalUpdate.bind(this)} 
                                removeMessage={this.removeMessage.bind(this)} 
                                sendMessage={this.addNewMessage.bind(this)} 
                                updateInterval={this.handleTimerIntervalUpdate.bind(this)} 
                                interval={timer_interval} 
                                repeat={timer_repeat} 
                                current_index={timer_current_index} 
                                messages={timer_messages}  />
                        )}
                        <div className={`OptionsBoard`}>
                            {removeAllConfirmation ? (
                                <div className={`RemoveAllConfirmation`}>
                                    <span>REMOVE ALL: Are you sure?</span>
                                    <button onClick={() => this.handleRemoveAll()}>YES</button>
                                    <button onClick={() => this.handleRemoveAllClick(true)}>NO</button>
                                </div>
                            ) : (
                                <React.Fragment>
                                    <button 
                                        disabled={tab === 'text' ? true : false} 
                                        onClick={() => this.handleTabChange('text')}>
                                        Text Messages
                                    </button>
                                    <button 
                                        disabled={tab === 'timer' ? true : false} 
                                        onClick={() => this.handleTabChange('timer')}>
                                        Timer Messages
                                    </button>
                                    <button 
                                        onClick={() => this.handleRemoveAllClick(false)}>
                                        Remove All
                                    </button>
                                </React.Fragment>
                            )}
                        </div>
                    </div>
                </div>
            )
        }  else {
            return (
                <div className="Config">
                    <div className={`LiveConfigPage ${this.state.theme === 'light' ? 'LiveConfigPage-light' : 'LiveConfigPage-dark'}`}>
                        Loading
                    </div>
                </div>
            )
        }
    }
}