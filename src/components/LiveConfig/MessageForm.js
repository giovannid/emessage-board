import React from 'react'

export default class MessageForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
    }
    
    handleInputChange(e){
        this.setState({ value: e.target.value });
    };
    
    handleBlur() {
        this.setState({ isFocused: false });
    };
    
    handleFocus() {
        this.setState({ isFocused: true });
    };

    handleSubmitClick() {
        if(this.state.value.trim() !== '') {
            this.props.sendMessage(this.state.value, this.props.timer_queue);
            this.setState({
                value: ''
            });
        }
    };
    
    render() {
        const { value, isFocused } = this.state;
        return (
            <div className="MessageForm">
                <input
                    tabIndex={1}
                    type="text"
                    className={`TextField ${isFocused ? `focused` : `TextField`}`}
                    placeholder="Type something..."
                    value={value}
                    onChange={this.handleInputChange.bind(this)}
                    onFocus={this.handleFocus.bind(this)}
                    onBlur={this.handleBlur.bind(this)}
                />
                <button tabIndex={-1} onClick={this.handleSubmitClick.bind(this)} className={`EditButton ControlButtons`}>Send</button>
            </div>
        );
    }
}