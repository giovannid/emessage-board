# EMessage Board Twitch.Tv Extension

Display a separate chat on your stream that can be used to send messages to your viewers.

![Screenshot1](https://static-cdn.jtvnw.net/jtv_user_pictures/uesoakkk34idj3bh7jbhqur1sprunb/0.0.1/screenshotda0210f1-f693-4d0e-a68e-c114dda60681)

[Check out the extension on Twitch.Tv](https://www.twitch.tv/ext/uesoakkk34idj3bh7jbhqur1sprunb-0.0.1)
